﻿using System;
using System.Collections.Generic;

namespace LoomaAed
{
    class Program
    {
        static void Main(string[] args)
        {
            //Loom loom1 = new Loom { Liik = "krokodill" }; // default konstruktor ja initializer
            Loom loom2 = "Krokodill"; // casting operaator
            Loom loom3 = new Loom ("Krokodill"); // parameetriga konstruktor
            Loom loom4 = Loom.ConvertToLoom("krokodill");
            //loom1.MakeNoise();
            ((Loom)"potsataja").MakeNoise();
            // erinevad viisid looma tekitamiseks

            KoduLoom kusti = new KoduLoom("Kusti")  { Liik = "oinas" };
            //kusti.MakeNoise();

            Kass kass = new Kass("Miisu") {  Tõug = "angoora" };

            //foreach (Loom l in Loom.Loomaaed) l.MakeNoise();

            kass.MakeNoise(); // 1. kord kräunub
            kass.Silita();
            kass.MakeNoise(); // 2. kord nurrub
            kass.Sikuta();
            kass.MakeNoise(); // 3. kord kräunub jälle

            Koer pauka = new Koer("Pauka");
            pauka.MakeNoise();
            Koer polla = new Koer("Polla");
            pauka.MakeNoise();

            Sepik s = new Sepik();

            Lõuna(s);
            Lõuna(pauka);
            Lõuna(kass);


        }

        public static void Lõuna(object x)
        {
            //if (x is ISöödav i) i.Süüakse();
            //else if (x is Loom l) l.MakeNoise();

            (x as ISöödav)?.Süüakse();
        }
    }



    class Loom 
    {
        public static List<Loom> Loomaaed = new List<Loom>(); 
        public string Liik { get; init; }
        public Loom(string liik)
        {
            Loomaaed.Add(this);
            Liik = liik; }
        public Loom() : this("tundmatu loom") { }
        public virtual void MakeNoise() 
            => Console.WriteLine($"{Liik} teeb koledat häält");
        public static implicit operator Loom(string liik) 
            => new Loom(liik);
        public static Loom ConvertToLoom(string liik) => new Loom(liik);




    }

    class KoduLoom : Loom
    {

        public string Nimi { get; init; } = "nimeta";

        public KoduLoom(string nimi) : base("pudulojus") => Nimi = nimi;

        public override void MakeNoise() => Console.WriteLine($"{Liik} {Nimi} teeb mahedat häält");
    }

    class Kass : KoduLoom
    {
        public string Tõug { get; init; } = "tõuta";

        private bool tuju = false;

        public Kass(string nimi) : base (nimi) { Liik = "kass"; }

        public void Silita() => tuju = true;
        public void Sikuta() => tuju = false;

        public override void MakeNoise()
        {
            Console.WriteLine(
                tuju ? $"Kass {Nimi} lööb nurru" : $"Kass {Nimi} kräunub"
                
                );
        }

    }

    class Koer : KoduLoom, ISöödav, IComparable<Koer>
    {
        public Koer(string nimi) : base(nimi) { Liik = "koer"; }

        public int CompareTo(Koer other)
        {
            return this.Nimi.CompareTo(other.Nimi);
        }

        public override void MakeNoise()
        {
            Console.WriteLine($"Koer {Nimi} haugub");
        }

        public void Süüakse()
        {
            Console.WriteLine($"Koer {Nimi} pannakse nahka") ;
        }
    }

    interface ISöödav
    {
        public void Süüakse();
    }

    class Sepik : ISöödav
    {
        public void Süüakse()
        {
            Console.WriteLine("keegi nosib sepikut"); 
        }
    }


}
