﻿using System;
using System.Collections.Generic;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace NWConsoleCore
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            string connectionString = @"Data Source=(localdb)\mssqllocaldb;Initial Catalog=Northwind;Integrated Security=True";

            NW db = new NW();
            //foreach (var p in db.Products)
            //    Console.WriteLine(p.ProductName + " " + p.CategoryID);

            Console.WriteLine("kalatooted");

            foreach (var p in db.Categories
                        .Include("Products")
                        .Where(x => x.CategoryID == 8)
                        .FirstOrDefault()
                        ?.Products.ToList())
                Console.WriteLine(p.ProductName);

            Console.WriteLine("kunded");
            db.Customers
                .Select(x => new { x.CompanyName, x.ContactName })
                .ToList()
                .ForEach(x => Console.WriteLine(x));

            Console.WriteLine("töötajad");
            db.Employees
                .Select(x => new { x.FirstName, x.LastName })
                .ToList()
                .ForEach(x => Console.WriteLine(x));


        }
    }

    public class NW : DbContext
    {

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(@"Data Source = (localdb)\mssqllocaldb; Initial Catalog = Northwind; Integrated Security = True");
            }
        }

        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<Category> Categories { get; set; }

        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }



    }

    public class Product
    {
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public decimal? UnitPrice { get; set; }

        public int? CategoryID { get; set; }

        public virtual Category Category { get; set; }
    }

    public class Category
    {
        public Category() => Products = new HashSet<Product>();

        public int CategoryID { get; set; }
        public string CategoryName { get; set; }

        public ICollection<Product> Products { get; set; } 
    }

    public class Employee
    {
        public int EmployeeID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    public class Customer
    {
        public string CustomerID { get; set; }
        public string CompanyName { get; set; }
        public string ContactName { get; set; }
    }
    
}
