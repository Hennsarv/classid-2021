﻿using System;
using System.Collections.Generic;

namespace MisOnKlass
{
    class Program
    {
        static void Main(string[] args)
        {
            var arvud1 = new int[3] { 1, 2, 3 };
            int[] arvud2 = new int[] { 1, 2, 3 };
            int[] arvud3 = { 1, 2, 3 };
            var list1 = new List<int>() { 1, 2, 3 };
            List<int> list2 = new List<int> { 1, 2, 3 };



            Console.WriteLine("\nclass is type & type is class\n");
            
            
            // classi tüüpi muutuja sisaldab objekti, mis vastab sellele kirjeldusele
            Inimene henn = new Inimene();   // inimese tüüpi muutuja
            henn.Nimi = "Henn Sarv";        // inimese väljade(muutujate) poole
            henn.Vanus = 66;                // . on tehe nimega MemberCall
            Console.WriteLine(henn);

            Inimene ants = new Inimene()    // new teeb mälus pesa,
                                            // kuhu see inimene ära mahub oma andmetega
            {
                Nimi = "Ants",
                Vanus = 40
            };
            Console.WriteLine(ants);

            Inimene sarvik = henn;  // class puhul omistamine on mäluaadressi (ref) kopeerimine
                                    // kaks muutujat viitavad samale kohale mälus
                                    // class on reference tüüpi asi
                                    // struct on value tüüpüi asi
                                    // muutuja sees on 'kohe' väljad
                                    // structi puhul omistamine on andmete (value) kopeerimine
            sarvik.Nimi = "Sarviktaat";

            Console.WriteLine(henn);

            Inimene peeter1 = new Inimene() { Nimi = "Peeter", Vanus = 28 };
            Inimene peeter2 = new Inimene { Nimi = "Peeter", Vanus = 28 };
            //Inimene peeter3 = new { Nimi = "Peeter", Vanus = 28 };  // tuleb C#9-s

            

        }
    }

    // class on kirjeldus, definitsioonide jada, mis kirjeldab sedalaadi objekte
    class Inimene
    {
        // sõnad public ja static jpm selguvad meile pisut hiljem
        // võta neid hetkel nii nagu nad on

        // klassi muutujad
        public string Nimi;
        public int Vanus;
       
        // klassi meetodi ja funktsioonid
        // Selle rea lahtiseletamine seisab meil veel ees
        public override string ToString() => $"inimene {Nimi} vanusega {Vanus}";

    }

}
