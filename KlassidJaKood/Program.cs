﻿using System;
using Henn.StandardKlassid;


namespace KlassidJaKood
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Inimene inimene = new() { Nimi = "Henn", SünniAeg = new DateTime(1955,03,07)};

            Console.WriteLine(inimene);
        }
    }
  
}


