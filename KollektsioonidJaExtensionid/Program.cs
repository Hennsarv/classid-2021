﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace KollektsioonidJaExtensionid
{

    static class E
    {
        public static string MyJoin<T>(this IEnumerable<T> m, string sep = " ")
            => string.Join(sep, m);

        public static void Print<T>(this T t) => Console.WriteLine($"{t.GetType().Name}: {t}");

        public static IEnumerable<int> Paaris(this IEnumerable<int> m)
        {
            foreach (var x in m)
            {
                if (x % 2 == 0) yield return x;
            }
        }
        public static IEnumerable<int> Paaritu(this IEnumerable<int> m)
        {
            foreach (var x in m)
            {
                if (x % 2 == 1) yield return x;
            }
        }

        public static IEnumerable<T> Millised<T>(this IEnumerable<T> m, Func<T,bool> f)
        {
            foreach (T x in m) if (f(x)) yield return x;
        }

    }

    class Program
    {
        //static void Vaheta(ref int x, ref int y) => (x, y) = (y, x);
        //static void Vaheta(ref double x, ref double y) => (x, y) = (y, x);
        // generic asi

        public static bool KasJagubKolmega (int x) =>  x % 3 == 0;

        static void Vaheta<T>(ref T x, ref T y) => (x, y) = (y, x);

        static void Main(string[] args)
        {
            int[] arvud = { 1, 7, 2, 3, 8, 4, 6, 0, 7 };

            //for(var e = arvud.GetEnumerator(); e.MoveNext();)
            //{
            //    Console.WriteLine(e.Current);
            //}

            //Func<int, bool> kas = (int x) => { return x % 7 == 0; };
            // kas = Program.KasJagubKolmega;


            //foreach (var x in arvud) Console.Write($"{x}, ");
            //Console.WriteLine(string.Join(", ", arvud));
            arvud.MyJoin(", ").Print();
            arvud
                //.Paaritu()
                //.Paaris()
                //.Where( (int x) => x % 7 == 0 )
                .Skip(10)
                //.Take(3)
                .Select( x => x * x)
                .DefaultIfEmpty()
                .Average()
                //.MyJoin(", ")
                .Print();

            (47 + 23).Print();



            Nullable<int> arva = arv;
            if (DateTime.Today.DayOfWeek == DayOfWeek.Tuesday) arv = 77;
            arv?.Print(); // if (arv != null) arv.value.Print();
            (arv??0).Print(); // (arv == null ? 0 : arv.Value).Print();



        }
    }
}
