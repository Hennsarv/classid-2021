﻿using System;
using System.Collections.Generic;
using System.IO;

namespace KeerulineInimene
{
    class Program
    {
        static void Main(string[] args)
        {
            //Mast m = (Mast)3;
            //m--;
            //Console.WriteLine(m);


            List<Person> people = new List<Person>();
            string filename = @"..\..\..\Inimesed.txt";
            foreach(var r in File.ReadAllLines(filename))
            {
                people.Add(
                    new Person 
                    { 
                        Name = r.Split(',')[0], 
                        PI = r.Split(',')[1] 
                    }
                    );
            }
            foreach (var p in people) Console.WriteLine(p);

            Person henn = people[0];
            //Console.WriteLine(henn.PI);
            //henn.SetSalary(10000);
            //Console.WriteLine(henn);
            //henn.SetSalary(5000);
            //Console.WriteLine(henn);

            henn.Salary = 10000; // henn.SetSalary(10000)
            Console.WriteLine(henn.Salary);  // henn.GetSalary()

            henn.Salary += 5000;
            // henn.SetSalary(henn.GetSalary()+5000);

            Console.WriteLine(henn.Gender);

            

        }
    }

    class Person
    {
        public string Name;
        public string PI; // isikukood saakkppxxxc

        // teeme sellest ka property:
        public DateTime BirtDate =>
            new DateTime(
                // PI[0] = 1,2 - 1800 = 3,4 - 1900 = 5,6 - 2000
                // (PI[0] < '3' ? 1800 : PI[0] < '5' ? 1900 : 2000) +
                ((PI[0] - '1') / 2 + 18)*100 +
                int.Parse(PI[1..3]),    // aasta
                int.Parse(PI[3..5]),    // kuu
                int.Parse(PI[5..7])    // päev
                );

        // readonly property
        //public Gender Gender { get => (Gender)(PI[0] % 2); }
        public Gender Gender => (Gender)(PI[0] % 2);

        public int Age; // Vanus => 

        private int salary; // sellele muutujale pääseb ligi ainult sellest klassist
        // funktsioon 
        public int GetSalary() => salary;
        //{
        //    return salary;
        //}
        // meetod
        public void SetSalary(int newSalary) 
            => salary = newSalary > salary ? newSalary : salary;
        //{
        //    // if (newSalary > salary) salary = newSalary;
        //    salary = newSalary > salary ? newSalary : salary;
        //}


        // property
        public int Salary
        {
            //get { return salary; }
            //set { salary = value > salary ? value : salary; }

            get => salary;
            set => salary = value > salary ? value : salary; 
        }

        public override string ToString() => $"{Gender} {Name} saab palka {salary} on sündinud {BirtDate:dd.MM.yyyy}";
    }

    enum Mast { Risti, Ruutu, Ärtu, Poti }

    // enum - on arvu andmetüüp
    // kus arvudele on antud nimed
    // Gender.Female = 0; Gender.Male = 1;
    enum Gender { Naine, Mees} 

}
