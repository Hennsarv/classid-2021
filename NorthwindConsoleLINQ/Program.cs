﻿using System;
using System.Linq;

namespace NorthwindConsoleLINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            
            using (NorthwindEntities db = new NorthwindEntities())
            {
                //db.Database.Log = Console.WriteLine;

                //foreach (var p in db.Products
                //    .Where(p => !p.Discontinued)
                //    .Where(p => p.UnitPrice < 20)
                //    .Select(p => new {p.ProductName, p.UnitPrice})
                //    )
                //{
                //    Console.WriteLine(p);
                //}

                // Extension functions
                var q0 = db.Products
                    .Where(p => !p.Discontinued)
                    .Where(p => p.UnitPrice < 20)
                    .OrderByDescending(x => x.UnitPrice)
                    .Select(p => new { p.ProductName, p.UnitPrice, Laoseis = p.UnitPrice * p.UnitsInStock })
                //    .ToList()
                    ;

                // LINQ - Language INtyegrated Query 
                var q1 = from p in db.Products
                         where !p.Discontinued
                         where p.UnitPrice < 20
                         orderby p.UnitPrice descending
                         select new { p.ProductName, p.UnitPrice, Laoseis = p.UnitPrice * p.UnitsInStock }
                         ;

                //foreach (var p in q1) Console.WriteLine(p);

                var kalad = db.Categories.Find(8);
                var liks = db.Products.Find(76);
                Console.WriteLine(liks?.ProductName??"toodet pole");

                //liks.UnitPrice += 10;
                kalad.CategoryName = "Seafood";
                db.SaveChanges();

                var liks2 = 
                    db.Products
                    .Where(p => p.ProductName == "Chai")  // võib tulla mitu aga ka 0
                    .FirstOrDefault(); // oleks ainult 1 ja kui ei leita, siis null
                Console.WriteLine(liks2?.UnitPrice??0M);

            }
        }
    }

}
