﻿using System;

namespace VeelPaarAsja
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hakkame jagama!");

            try
            {
                for (int i = 0; i < 5; i++)
                {
                    try
                    {
                        
                        Console.Write("anna üks arv: ");
                        int yks = int.Parse(Console.ReadLine());

                        if (yks == 7) throw new Exception("seitset me ei jaga");

                        Console.Write("anna teine arv: ");
                        int teine = int.Parse(Console.ReadLine());
                        if(teine != 0)
                        Console.WriteLine($"{yks} jagatud {teine} on {yks / teine}");
                        else Console.WriteLine("ise oled null");
                    }
                    catch (DivideByZeroException e)
                    {
                        Console.WriteLine("nulliga ei saa jagada");
                        throw;
                    }
                    catch (OverflowException)
                    {
                        Console.WriteLine("liiga suur arv");
                    }
                    catch (FormatException)
                    {
                        Console.WriteLine("vigane arv");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("miski asi on vale");
                        Console.WriteLine(e.Message);
                        Console.WriteLine(e.GetType().Name);
                    }
                    finally
                    {
                        Console.WriteLine("jagamine otsas");
                    }

                }

            }
            catch (Exception)
            {
                Console.WriteLine("kui te ei oska, siis mäng läbi");
               
            }        
        }
    }
}
