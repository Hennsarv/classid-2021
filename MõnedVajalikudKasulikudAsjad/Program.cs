﻿using System;
using System.Collections.Generic;
using System.IO;

namespace MõnedVajalikudKasulikudAsjad
{
    class Inimene
    {
        public string Nimi;
        public int Vanus;

        public override string ToString() => $"inimene {Nimi} vanusega {Vanus}";
    }

    class Program
    {
        static void Main(string[] args)
        {
            // Mõned hääd tehted stringidega
            string demo = "Henn on tore poiss";
            Console.WriteLine(demo.ToUpper());
            Console.WriteLine(demo.ToLower());
            Console.WriteLine(demo.Substring(5));
            Console.WriteLine(demo.Substring(5,7));
            Console.WriteLine(demo[5..12]);

            string eesnimi = "Henn";
            string perenimi = "Sarv";
            Console.WriteLine( (eesnimi + " " + perenimi).ToUpper()   );


            char x = demo[0]; // 'H'

            int[] arvud = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

            // range tehte .. näited
            Console.WriteLine("\n3 kuni 7 (excl)");
            foreach (var a in arvud[3..7]) Console.Write($"{a}, "); // 3 kuni 7 (excl)
            Console.WriteLine("\n3 kuni lõpuni");
            foreach (var a in arvud[3..]) Console.Write($"{a}, ");  // 3 kuni lõpuni
            Console.WriteLine("\nalgusest 5-ni (excl)");
            foreach (var a in arvud[..5]) Console.Write($"{a}, ");  // algusest kuni 5ni (excl)
            Console.WriteLine("\nalgusest üleeelviimaseni");
            foreach (var a in arvud[..^2]) Console.Write($"{a}, "); // algusest kuni üleelviimaseni



            // split demo
            Console.WriteLine("\nkuidas kasutada splitti");
            var sõnad = demo.Split(' '); // vaata seal sulgudes on char ('' vahel, mitte "" vahel)
            foreach (var sõna in sõnad) Console.WriteLine(sõna);

            string email = "henn@koolitus.ee";
            string domain = email.Split('@')[1].Split('.')[0];
            Console.WriteLine($"meiliaadress:{email} domain:{domain}");

            var textArvud = string.Join(", ", arvud);
            Console.WriteLine(textArvud);

            Console.WriteLine("\n3 kuni 7 (excl)");
            Console.WriteLine(string.Join(", ", arvud[3..7]));
            Console.WriteLine("\n3 kuni lõpuni");
            Console.WriteLine(string.Join(", ", arvud[3..]));
            Console.WriteLine("\nalgusest 5-ni (excl)");
            Console.WriteLine(string.Join(", ", arvud[..5]));
            Console.WriteLine("\nalgusest üleeelviimaseni");
            Console.WriteLine(string.Join(", ", arvud[..^2]));

            // mõned vajalikud oskused failidega
            // siis läheb meil edasi elu lihtsamaks

            // mingi sisu kirjutamine faili
            // failidega toimetamiseks tuleks üles lisada 
            // using System.IO;
            // NB! et ei peaks backslash \ märki topeldama, pane stringi ette @
            string filename = @"c:\demo\test.txt";
            File.WriteAllText(filename, demo); // kirjutame selle string hoopis faili

            // stringi massiivi kirjutamine faili
            string[] nimed = { "Henn", "Ants", "Peeter" };
            File.WriteAllLines(@"c:\demo\nimed.txt", nimed);

            // kui ma tahan faili kirjutada mitte korraga vaid ridahaaval
            StreamWriter tw = File.CreateText(@"c:\demo\tw.txt");  // nii saan teha "kirjutaja"
            foreach (var a in arvud) tw.WriteLine(a);  // ja nüüd kasutan teda nagu consooli
            tw.Flush();  // lõpuks tuleks teha flush - muidu jääb mällu
            tw.Close();  // lõpuks on viisakas sulgeda

            // kui File.WriteAllText / .WriteAllLines kirjutab faili üle
            // siis .AppendAllText kirjutab failile sappa
            File.AppendAllText(@"c:\demo\tw.txt", "--- selle lisasin lõppu ---");

            // fili sisu lugemine stringi massiiviks
            //var read = File.ReadAllLines(@"c:\demo\demo.txt");
            //for (int i = 0; i < read.Length; i++)
            //{
            //    Console.WriteLine($"rida {i:00}. {read[i]}");
            //}

            // kus pidada faili (mis on sobilik kaust)
            // projektikaust
            string projFolder = @"..\..\..\"; 
            // see on programmikaustast 3 sammu ülespoole
            // .net framework projektis on 2 sammu ülespool
            string projFile = projFolder + "andmed.txt"; // failinimi projektikaustas

            List<Inimene> rahvas = new List<Inimene>();
            foreach (var a in File.ReadAllLines(projFile))
            {
                var osad = (a + ",,,").Split(',');
                string nimi = osad[0];
                int vanus = int.TryParse(osad[1], out int v) ? v : 0;
                rahvas.Add(new Inimene { Nimi = nimi, Vanus = vanus });
            }

            foreach (var i in rahvas) Console.WriteLine(i);
                





        }
    }
}
