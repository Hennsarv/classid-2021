﻿using System;

namespace Henn.StandardKlassid
{
    public partial class Inimene
    {
        public string Nimi;
        public DateTime SünniAeg;

        public override string ToString() => $"Inimene {Nimi} sündinud {SünniAeg:dd.MMMM.yyyy}";
        
    }
}
