﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;



namespace ÕpilasedÕpetajad
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Inimene> koolipere = new List<Inimene>
            { new() {Nimi = "Henn Sarv", IK = "35503070211" } };

            string kaust = @"..\..\..\"; // projekti kaust programmist vaadates
            string õpilasedFile = kaust + "õpilased.txt";
            string õpetajadFile = kaust + "õpetajad.txt";
            // mõned muutujad nimekiri, kaust ja failinimed
            Console.WriteLine("Meie koolipere:");
            // kaks tsüklit - õpilaste ja õpetajate sisselugemiseks
            #region esimene variant tsükliga
            //foreach (var r in File.ReadAllLines(õpilasedFile))
            //{
            //    if (r.Trim() != string.Empty)
            //    {
            //        var o = r.Split(',');
            //        koolipere.Add(
            //            new Õpilane
            //            {
            //                IK = o[0].Trim(),
            //                Nimi = o[1].Trim(),
            //                Klass = o[2].Trim()
            //            }
            //            );
            //    }
            //}
            //foreach (var r in File.ReadAllLines(õpetajadFile))
            //{
            //    if (r.Trim() != string.Empty)
            //    {
            //        var o = (r+",").Split(',');
            //        koolipere.Add(
            //            new Õpetaja
            //            {
            //                IK = o[0].Trim(),
            //                Nimi = o[1].Trim(),
            //                Aine = o[2].Trim(),
            //                Klass = o[3].Trim()
            //            }
            //            );

            //    }
            //}
            #endregion

            try
            {
                koolipere.AddRange(
                File.ReadAllLines(õpilasedFile)                 // õpilaste faili read
                    .Where(x => x.Trim() != string.Empty)       // jätame tühjad read vahele
                    .Select(x => x.Split(',').Select(y => y.Trim()).ToArray())
                    // kolme elemendiga massiivid
                    .Select(x => new Õpilane { IK = x[0], Nimi = x[1], Klass = x[2] })
                );

            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine("õpilaste faili es ole");
            }                        

            try
            {
                koolipere.AddRange(
        File.ReadAllLines(õpetajadFile)
            .Where(x => x.Trim() != string.Empty)
            .Select(x => (x + ",").Split(',').Select(y => y.Trim()).ToArray())
            .Select(x => new Õpetaja { IK = x[0], Nimi = x[1], Aine = x[2], Klass = x[3] })
            );

            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("õpside fail on jalutama läinud");
            }

            // seejärel üks tsükkel nende väljaprintimiseks
            foreach (var õ in koolipere
                .OrderBy(x => x.Nimi)
                .Select(x => x.Nimi)
                
                ) Console.WriteLine(õ);

            string koolipereFile = kaust + "koolipere.txt";

            File.WriteAllLines(koolipereFile,
            koolipere
                .Select(x =>
                    x is Õpetaja õ ? $"õpetaja,{õ.IK},{õ.Nimi},{õ.Aine},{õ.Klass}" :
                    x is Õpilane õp ? $"õpilane,{õp.IK},{õp.Nimi},{õp.Klass}" :
                    $"inimene,{x.IK},{x.Nimi},,"
                )
                );

        }
    }

    // esiteks teen vajalike väljadega kolm klassi
    // siis lisan neile ToString()

    class Inimene
    {
        public string IK { get; init; }
        public string Nimi { get; init; }
    }
    class Õpilane : Inimene
    {
        public string Klass { get; set; } // klass kus õpib

        public override string ToString() => $"{Klass} klassi õpilane {Nimi}";
    }
    class Õpetaja : Inimene
    {
        public string Aine { get; set; } // aine, mida õpetab
        public string Klass { get; set; } = ""; // klass, mida juhatab

        public override string ToString() => $"{Aine} õpetaja {Nimi}"
            + (Klass == string.Empty ? "" : $" (klassi {Klass} juhataja)")
            ;
    }
   
}
