﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace NorthWindConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            string connectionString = @"Data Source=(localdb)\mssqllocaldb;Initial Catalog=Northwind;Integrated Security=True";
            string command = "select productname, unitprice from products";
            
            using(SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                using(SqlCommand comm = new SqlCommand(command, conn))
                {
                    for(var R = comm.ExecuteReader() ; R.Read() ; )
                    {
                        Console.WriteLine(R["productname"]);
                    }
                }
            }

        }
    }
}
