﻿using System;
using System.Collections.Generic;
using System.IO;

namespace KlasssidKaks
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = @"..\..\..\inimesed.txt";
            var read = File.ReadAllLines(filename);
            //foreach (var rida in read) Console.WriteLine(rida);



            foreach (var rida in read)
            {
                var osad = rida.Split(',');
                //var i = new Inimene(osad[0]) {   Eesnimi = osad[1], Perenimi = osad[2] };
                var i = Inimene.Create(osad[0], osad[1], osad[2]);
            }

            //new Inimene("35503070211") { Eesnimi = "Sarviktaat" };
            Inimene.Create("35503070212", "Sarviktaat", "");

            Console.WriteLine(Inimene.InimesteArv);

            foreach (var x in Inimene.Inimesed) Console.WriteLine(x);

            //Inimene ihii = Inimene.GetByIK("35503070211");
            //if (ihii == null) Console.WriteLine("sellist meil ei ole");
            //else Console.WriteLine(ihii.Eesnimi);

            Console.WriteLine(Inimene.GetByIK("35503070211")?.Eesnimi??"pole sellist");

            Inimene.GetByIK("35503070212")?.Print();

        }
    }

    class Inimene
    {
        static List<Inimene> inimesed = new List<Inimene>();
        public static IEnumerable<Inimene> Inimesed => inimesed.AsReadOnly();

        static Dictionary<string, Inimene> inimesedDict = new Dictionary<string, Inimene>();

        public static int InimesteArv { get; private set; } = 0;

        public int Nr { get; private set; } = ++InimesteArv;
        public string Eesnimi { get; set; }
        public string Perenimi { get; set; }
        public string IK { get; init; }

        private Inimene(string ik) : this()
        {
            if(!inimesedDict.ContainsKey(ik)) inimesedDict.Add(ik, this);
            IK = ik;
        }

        public Inimene()
        {
            inimesed.Add(this);
        }

        public override string ToString() => $"{Nr}. {Eesnimi} {Perenimi} (ik: {IK})";

        public void Print() => Console.WriteLine(this);

        public static Inimene GetByIK(string ik) => 
            inimesedDict.ContainsKey(ik) ? inimesedDict[ik] : null;


        // factory meetod
        // staatiline meetod, mis kutsub välja privaat-konstruktori
        public static Inimene Create(string ik, string eesnimi, string perenimi) =>
            inimesedDict.ContainsKey(ik) ? null : new Inimene(ik) {Eesnimi = eesnimi, Perenimi = perenimi };

        
    }
}
