﻿using System;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Linq;


namespace NorthWindConsoleCore
{
    class Program
    {
        public static string NwConnectionString = @"Data Source=(localdb)\mssqllocaldb;Initial Catalog=Northwind;Integrated Security=True";

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            if (false)
            {
                using (SqlConnection conn = new SqlConnection(NwConnectionString))
                {
                    conn.Open();
                    string commandString = "select productname, unitprice from products";
                    using (SqlCommand comm = new SqlCommand(commandString, conn))
                    {
                        for (var R = comm.ExecuteReader(); R.Read();)
                            Console.WriteLine(R["productname"]);
                    }
                }

            }

            using (NW db = new NW())
            {
                Console.WriteLine("töötajad");
                db.Employees
                    .Select(x => new {x.FirstName, x.LastName})
                    .ToList()
                    .ForEach(x => Console.WriteLine(x))
                    ;

                Console.WriteLine("kunded");
                db.Customers
                    .Select(x => new { x.CompanyName, x.ContactName, x.Country })
                    .ToList()
                    .ForEach(x => Console.WriteLine(x))
                    ;
                Console.WriteLine("tooted");
                db.Products
                    .ToList()
                    .Select(x => new {x.ProductName, x.UnitPrice, x.CategoryID})
                    .ToList()
                    .ForEach(x => Console.WriteLine(x))
                    ;
                Console.WriteLine("kalatooted");
                db.Categories
//                    .Include("Products")
//                    .Where(x => x.CategoryID == 8)
//                    .FirstOrDefault()
                    .Find(8)
                    ?.Products
                    .Select(x => x.ProductName)
                    .ToList()
                    .ForEach(x => Console.WriteLine(x))
                    ;

            }


        }
    }

   
}
