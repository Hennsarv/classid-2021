﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NWAken
{
    public partial class Form1 : Form
    {
        static NorthwindEntities db = new NorthwindEntities();

        public Form1()
        {
            InitializeComponent();
            this.comboBox1.DataSource =
                 (new string[] { "*"}).Union(
                db.Categories.Select(x => x.CategoryName)
                )
                .ToList()
                ;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.dataGridView1.DataSource = db.Products.ToList();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.dataGridView1.DataSource = null;

            this.dataGridView1.DataSource = 
            db.Products
                .Where(x => this.comboBox1.Text == "*" ||  x.Categories.CategoryName == this.comboBox1.Text)
                .ToList();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            db.SaveChanges();
        }
    }
}
