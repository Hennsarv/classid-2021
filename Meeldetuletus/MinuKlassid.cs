﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meeldetuletus
{
    static class E
    {
        public static void Print(this Person p) => Console.WriteLine(p);

        public static DateTime PicToDateTime(this string pic)
            => DateTime.TryParseExact((pic+"0")[..1] switch
            {
                "1" => "18",
                "2" => "18",
                "3" => "19",
                "4" => "19",
                _ => "20"
            } + (pic+"0000000")[1..7], "yyyyMMdd"
                , new CultureInfo("en-US")
                , DateTimeStyles.None
                , out DateTime dt) ? dt : DateTime.Today;

        public static string Totitle(this string name) 
            => name == string.Empty ? "tundmatu" :            
            name[..1].ToUpper() + name[1..].ToLower();

        // ma lisasin mõned extensionid - hiljem seletan
        public static string ToTitle(this string name)
            => name
                   .Replace("-", "- ")
                   .Split(' ')
                   .Select(x => x == "" ? "" : x[..1].ToUpper() + x[1..].ToLower())
                   .JoinX()
                   .Replace("- ", "-")
            ;

        public static string JoinX(this IEnumerable<string> m, string sep = " ") 
            => string.Join(sep, m);

    }

    enum Gender { Female, Male, Unknown = 99 }
    enum Mast { Risti, Ruutu, Ärtu, Poti, Pada = 3 }


    class Person
    {

        // Kõigepealt väljad (enamasti private)
        static int vanusepiir = 18;
        static List<Person> people = new List<Person>();
        static int peopleCount = 0;
        public static int PeopleCount => peopleCount;

        // kui on static väljasid, siis need kõigepealt

        private int number = ++peopleCount;
        private string firstName;
        private string lastName;
        private string pic;
        private DateTime birthDate;

        //private string pIC; // isikukood 11 märki cyymmddxxxc
        //public string PIC
        //{
        //    get { return pIC; }
        //    set { pIC = value; }
        //}

        // siia vahele mõtleme veel mõned asjad - konstruktorid
        // aga seda juba peale lõunat

        public Person() // selline asi on kosntruktor
        {
            people.Add(this);
        }



        // edasi propertyd - mingi loogiline järjekord,
        // aga võiks olla lihtsamalt keerulisemale
        //public string PIC { get; init; } = ""; // hästi lihtne property
        // kui set asemele on init, siis see tähendab, et tohib ainult initsialiseerimisel
        // seda propertit ette anda
        // new Person {PIC = "......"} on lubatud
        // Person p = new Person {}; p.PIC = "....." keelatud

        // see lause saab meil kohe ka selgemaks
        // aga usume seda
        public static IEnumerable<Person> People => people.AsEnumerable();


        // property, mida saab ainult init ajal väärtustada
        public string PIC
        {
            get => pic;
            init => (pic,birthDate) = (value,value.PicToDateTime());
        }

        // FirstName kasutame eesnime tähenduses
        public string FirstName
        {
            get => firstName;
            //{
            //    return firstName;
            //}
            set => firstName = value.ToTitle();
            #region komment
            //{
            //    firstName = value.ToTitle();
            //    //firstName = value == string.Empty ? "tundmatu" :
            //    //    value[..1].ToUpper() +
            //    //    value[1..].ToLower();
            //} 
            #endregion
        }

        public string LastName
        {
            get => lastName;
            set => lastName = value.ToTitle();
        }

        // siis võiks olla readonly propertyd

        public string FullName => $"{firstName} {lastName}";
        public string Fullname { get => $"{firstName} {lastName}"; }
        public Gender Gender => 
            PIC == string.Empty ? Gender.Unknown : 
            (Gender)(PIC[0] % 2);

        public DateTime BirthDate => birthDate;
        //public DateTime BirthDate => PIC == string.Empty ? DateTime.Today :
        //    new (
        //            ((PIC[0]-'1') / 2 + 18) * 100 +
        //            (int.TryParse((PIC+"0000000")[1..3], out int a ) ? a : 1),
        //            (int.TryParse((PIC+"0000000")[3..5], out int k ) ? k : 1),
        //            (int.TryParse((PIC+"0000000")[5..7], out int p ) ? p : 1)

        //        );

        //public DateTime Birthdate =>
        //    DateTime.TryParseExact((pic+"00000000")[..1] switch
        //    {
        //        "1" => "18",
        //        "2" => "18",
        //        "3" => "19",
        //        "4" => "19",
        //        _ => "20"
        //    } + pic[1..7], "yyyyMMdd"
        //        , new CultureInfo("en-US")
        //        , DateTimeStyles.None
        //        ,  out DateTime dt) ? dt : DateTime.Today;



        public int Age => (DateTime.Today - birthDate).Days * 4 / 1461;
        

        #region polevaja
        // funktsioon getter
        public string GetFirstName() => firstName;
        // meetod setter
        public void SetFirstName(string newName)
        //=>      firstName = newName == string.Empty ? "tundmatu" :
        //        newName[..1].ToUpper() +
        //        newName[1..].ToLower();
        => newName.ToTitle();

        public void SetLastName(string newName) => newName.ToTitle();
        //{
        //    if (newName == string.Empty) lastName = "tundmatu";
        //    else lastName =
        //       newName[..1].ToUpper() +
        //       newName[1..].ToLower();
        //}

        #endregion

        // ja seeejärel juba keerukamadf meetodi ja funktsioonid
        // saab ka ilma nendeta

        public bool CanDrinkAlcohol() => Age >= vanusepiir;


        // kõige lõpus overraidid
        public override string ToString() => $"{number}. {Gender} person: {FullName} born: {BirthDate: dd.MM.yyyy} age: {Age}";

 



    }
}
