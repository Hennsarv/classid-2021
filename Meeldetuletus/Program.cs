﻿using System;
using System.Collections.Generic;

namespace Meeldetuletus
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Inimese näited
            ////Inimene henn = new Inimene();
            ////henn.Nimi = "Henn Sarv"; // member call tehe
            ////henn.Vanus = 65;

            ////henn.Vanus++;

            ////initsialiseerimise avaldis
            ////Inimene ants = new Inimene { Nimi = "Ants Saunamees", Vanus = 40 };

            ////Inimene[] inimesed = new Inimene[10];
            ////inimesed[0] = henn;
            ////inimesed[1] = ants;
            ////inimesed[2] = new Inimene { Nimi = "Peeter Suur", Vanus = 28 };

            ////List<Inimene> inimesed = new List<Inimene>
            ////{
            ////    henn,
            ////    ants,
            ////    new Inimene {Nimi = "Peeter Suur", Vanus = 28},


            ////}; 
            ///
            //Inimene inimene = new Inimene { Nimi = "Henn", Vanus = 66 };
            //Console.WriteLine(inimene);
            #endregion

            Person p = new Person {FirstName="Henn", LastName = "Sarv", PIC = "35503070211" };
 //           Console.WriteLine(p);

//            if (p.CanDrinkAlcohol()) Console.WriteLine($"joo sõõrsilm {p.FirstName}");

            Person teine = new Person() { FirstName = "Muhv" , PIC = "51802010000"};
            //           Console.WriteLine(teine);
            //           if (teine.CanDrinkAlcohol()) { } else { Console.WriteLine("sina juua ei saa"); }


            new Person { FirstName = "Toomas", LastName = "linnupoeg", PIC = "37703030000" };

            foreach (var x in Person.People) Console.WriteLine(x);



            
            //Console.WriteLine(p);
//            p.Print();
//            E.Print(p);


        }

        static int Liida(int yks, int teine)
        {
            return yks + teine;
        }

    }

    class Inimene
    {
        public string Nimi;
        public int Vanus;

        public override string ToString() => $"Inimene {Nimi} vanusega {Vanus}";
    }

 

}
